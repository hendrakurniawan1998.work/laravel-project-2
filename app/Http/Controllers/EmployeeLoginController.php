<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class EmployeeLoginController extends Controller
{
    protected $redirectTo = '/home';

    public function __construct()
    {
        $this->middleware('guest:employee')->except('logout');
    }

    public function showLoginForm($company_name)
    {
        if(Auth::check())
        {
            return redirect("home");
        }

        error_log("SHOW LOGIN FORM: COMPANY NAME=".$company_name);

        return view('employee.login')->with("company_name", $company_name);
    }

    public function login(Request $request, $company_name)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);


        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {

            error_log("EMPLOYEE LOGIN: User Logged In Successfully");

            error_log("EMPLOYEE LOGIN: EMPLOYEE EMAIL=" . Auth::user()->email);

            error_log("EMPLOYEE LOGIN: EMPLOYEE COMPANY NAME=" . Auth::user()->company->name);

            if (Auth::user()->company->name == $company_name) {

                error_log("EMPLOYEE LOGIN: COMPANY NAME=" . $company_name);

                return redirect("home");
            }
            else
            {
                Auth::logout();

                Session::flush();
            }
        }

        return back()->withSuccess('Login details are not valid');
    }

    protected function guard()
    {
        return Auth::guard('employee');
    }

    public function signOut()
    {
        Session::flush();
        Auth::logout();

        return Redirect('login');
    }
}
