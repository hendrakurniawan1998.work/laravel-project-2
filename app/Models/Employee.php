<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Employee extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'first_name',
        'last_name', 
        'phone', 
        'email', 
        'company_id', 
        'created_by_id', 
        'updated_by_id', 
        'created_at',
    ];

    /**
     * The attributes that are guarded
     *
     * @var array
     */
    protected $guarded = ['id'];



    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function getAuthPassword()
    {
     return $this->password;
    }


    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
